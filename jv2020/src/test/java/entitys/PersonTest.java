package entitys;


import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import entitys.User.RoleUser;
import utils.EasyDate;

public class PersonTest {

	private static User personTest1; 
	private User personTest2; 


	@BeforeAll
	static void iniciarlizeGlobalData() {
		personTest1 = new User(
				new Nif("00000008P"), 
				"Pepe", 
				"Márquez Alón", 
				new Address("Alta", "10", "30012", "Murcia"), 
				new Mail("pepe@gmail.com"), 
				new EasyDate(1990, 11, 12),
				new EasyDate(2020, 02, 05), 
				new Password("Miau#32"), 
				RoleUser.ADMIN
				);
	}


	@AfterAll
	public static void clearGlobalData() {
		personTest1 = null;
	}

	@BeforeEach
	public void initializeTestData() {	
		this.personTest2 = new User();
	}

	@AfterEach
	public void clearTestData() {
		this.personTest2 = null;
	}

	// Test's CON DATOS VALIDOS

	@Test
	void testSetNif() {	
		// TODO Auto-generated method stub	
	}

	@Test
	void testSetName() {	
		this.personTest2.setName("Juan");
		assertEquals(this.personTest2.getName(), "Juan");
	}

	@Test
	void testSetSurnames() {	
		this.personTest2.setSurnames("Márquez Alón");
		assertEquals(this.personTest2.getSurnames(), "Márquez Alón");
	}

	@Test
	void testSetAddress() {	
		this.personTest2.setAddress(new Address ("3", "10", "30012", "Murcia"));
		assertEquals(this.personTest2.getAddress(),new Address());
	}

	@Test
	public void testSetEmail() {
		this.personTest2.setMail(new Mail ("pepe@gmail.com"));
		assertEquals(this.personTest2.getMail(), "pepe@gmail.com");

	}

	@Test
	void testSetBirthDate() {	

	}

	@Test
	void testClone() {
		// TODO Auto-generated method stub
	}


	@Test
	void testEqualsObject() {	
		// TODO Auto-generated method stub
	}


	// Test's CON DATOS NO VALIDOS

	@Test
	public void testSetNifNull() {
		// TODO Auto-generated method stub
	}

	@Test
	public void testSetNifNotValid() {
		// TODO Auto-generated method stub
	}

	@Test
	public void testSetNameNull() {
		try {
			this.personTest2.setName(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}

	@Test
	public void testSetNameNotValid() {
		try {
			this.personTest2.setName("n0 valid4");
			fail("No debe llegar aquí...");
		} 
		catch (EntitysException e) { 
		}
	}

	@Test
	public void testSetSurnamesNull() {
		try {
			this.personTest2.setSurnames(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}

	@Test
	public void testSetSurnamesNotValid() {
		try {
			this.personTest2.setSurnames("no válido");
			fail("No debe llegar aquí...");
		} 
		catch (EntitysException e) { 
		}
	}

	@Test
	public void testSetAddressNull() {
		try {
			this.personTest2.setAddress(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}

	@Test
	public void testSetAddressNotValid() {
		try {
			this.personTest2.setAddress(new Address("no válido","no válido", "no válido","no válido" ));
			fail("No debe llegar aquí...");
		} 
		catch (EntitysException e) { 
		}
	}

	@Test
	public void testSetMailNull() {
		// TODO Auto-generated method stub
	}

	@Test
	public void testSetMailNotValid() {
		// TODO Auto-generated method stub
	}

	@Test
	public void testSetBirthDateNull() {
		// TODO Auto-generated method stub
	}

	@Test
	public void testSetBirthDateNotValid() {
		// TODO Auto-generated method stub
	}


















} 
